import requests as rq
import random
DEBUG = False
class WBot:
    words = [word.strip() for word in open("5letters.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    creat_url = wordle_url + "create"
    guess_url = wordle_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WBot.creat_url, json=creat_dict)

        self.choices = [w for w in WBot.words[:]]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']
        
        while not won:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: list[str]):
        new_choices = []
        for word in self.choices:
            match = True
            for i, (c, f) in enumerate(zip(choice, feedback)):
                if f == 'G' and word[i] != c:
                    match = False
                    break
                elif f == 'Y' and (c not in word or word[i] == c):
                    match = False
                    break
                elif f == 'R' and c in word:
                    match = False
                    break
            if match:
                new_choices.append(word)
        self.choices = new_choices


game = WBot("CodeShifu")
game.play()
