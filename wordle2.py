import requests as rq
import json
print(rq.__version__)
base_url = "https://we6.talentsprint.com/wordle/game/"
register_url = base_url + "register"
create_url = base_url + "create"
guess_url = base_url + "guess"
def read_names_from_file(filename):
    with open(filename, 'r') as file:
        names = [name.strip() for name in file.readlines() if name.strip()]
    return names


def bot(player_name, filename):
    names = read_names_from_file(filename)

    register_dict = {"mode": "wordle", "name": player_name}
    register_with = json.dumps(register_dict)
    print(register_with)
    response = rq.post(register_url, json=register_dict)
    print(response)
    
    if response.status_code != 201:
        print(f"Failed to register. Status code : {response.status_code}")
        return
    my_id = response.json()['id']
    print(f'Player {player_name} with id {my_id} has been successfully registered')
    create_dict = {"id": my_id, "overwrite": True}
    response = rq.post(create_url, json=create_dict)
    print(response.json())
    
    session = rq.Session()
    resp = session.post(register_url , json = register_dict)
    print(resp.json())
    my_id = resp.json()['id']
    create_dict = { "id" : my_id, "overwrite": True}
    response = session.post(create_url, json=create_dict)
    print(response.json())   
    
    i = 0
    for name in names:
        while(i != 6):
                guess_data = {"id" : my_id, "guess": name.lower()}
                response = session.post(guess_url, json = guess_data)
                
                feedback = response.json().get('feedback')
                message = response.json().get('message')
        
                print(f'Guess: {name}, Feedback: {feedback},Message: {message}')
                if feedback == 'GGGGG':
                        print(f'You guessed the word {name} correctly.')
                        break
                else:
                        print("try again")

                if feedback == None:
                        to_remove = [letter for letter in name]
                        for i in to_remove:
                                for name in names:
                                        if i in name:
                                                names.remove(name)
                i += 1

bot("Ritigya","5letters.txt")

